package com.hyc.study.controller;

import com.hyc.study.service.TreadTasks;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@AllArgsConstructor
public class AsyncTaskUse {

    private final TreadTasks treadTasks;

    // http://localhost:8080/startMysync
    @GetMapping("/startMysync")
    public void startMyTreadTask() {
        treadTasks.startMyTreadTask();
    }

}
