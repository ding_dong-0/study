package com.hyc.study.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@ResponseBody
//@Controller
@RestController
public class HelloController {

// http://localhost:8088/hello
    @RequestMapping("/hello")
    public String handle01() {

        return "Hello SpringBoot2";
    }
}
