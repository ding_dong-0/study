package com.hyc.study.controller;

import com.hyc.study.anno.SysLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {


    @SysLog("测试日志注解")
    @GetMapping("/test")
    public String test(@RequestParam("name") String name) {
        return name;
    }


}
