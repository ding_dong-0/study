package com.hyc.study.service;


import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class TreadTasks {
    @Async
    public void startMyTreadTask() {
        System.out.println("this is my async task");
        //打印出调用的线程
        System.out.println(Thread.currentThread().getName());
    }


}
